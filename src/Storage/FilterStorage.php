<?php

namespace Redenge\Filter\Storage;

use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\Database\Context;


/**
 * Description of FilterStorage
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class FilterStorage
{

	const EXPIRE = '24 hours';

	/**
	 * @var Cache
	 */
	private $cache;

	/**
	 * @var Context
	 */
	private $db;


	public function __construct(IStorage $storage, Context $db)
	{
		$this->cache = new Cache($storage, __CLASS__);
		$this->db = $db;
	}


	/*public function getFilterGroups($languageId)
	{
		if (!($data = $this->cache->load($key = 'filterGroups/lang/' . $languageId))) {
			$data = $this->cache->save($key, function() use ($languageId) {
				$select = 'filter_group.id, filter_group.code, fga.name';
				$join = sprintf("JOIN filter_group_attribute fga ON filter_group.id = fga.filter_group_id AND fga.language_id =%d",
					$languageId);
				$sql = sprintf("SELECT %s FROM filter_group %s", $select, $join);

				return $this->db->query($sql)->fetchAll();
			}, [
				Cache::EXPIRE => self::EXPIRE,
				Cache::TAGS => $this->createFilterGroupsTag(),
			]);
		}

		return $data;
	}*/


	public function getFilterGroupByCode($groupCode, $languageId)
	{
		if (!($data = $this->cache->load($key = 'filterGroup/' . $groupCode . '/lang/' . $languageId))) {
			$data = $this->cache->save($key, function() use ($groupCode, $languageId) {
				$select = 'filter_group.id, filter_group.code, fga.name';
				$join = sprintf("JOIN filter_group_attribute fga ON filter_group.id = fga.filter_group_id AND fga.language_id =%d",
					$languageId);
				$filter = sprintf("filter_group.code = '%s'", $groupCode);
				$sql = sprintf("SELECT %s FROM filter_group %s", $select, $join, $filter);

				return $this->db->query($sql)->fetch();
			}, [
				Cache::EXPIRE => self::EXPIRE,
				Cache::TAGS => $this->createFilterGroupTag(null, $groupCode),
			]);
		}

		return $data;
	}


	public function getFilterItems($groupId, $languageId)
	{
		if (!($data = $this->cache->load($key = 'filterItems/' . $groupId . '/lang/' . $languageId))) {
			$data = $this->cache->save($key, function() use ($groupId, $languageId) {
				$select = 'filter.id, filter.code, fa.name';
				$join = sprintf("JOIN filter_attribute fa ON fa.filter_id = filter.id AND fa.language_id=%d",
					$languageId);
				$filter = 'filter.filter_group_id=' . (int) $groupId;
				$sql = sprintf("SELECT %s FROM filter %s WHERE %s", $select, $join, $filter);

				return $this->db->query($sql)->fetchAll();
			}, [
				Cache::EXPIRE => self::EXPIRE,
				Cache::TAGS => $this->createFilterItemsTag($groupId, $languageId),
			]);
		}

		return $data;
	}


	/**
	 * @return array
	 */
	/*private function createFilterGroupsTag()
	{
		return ['filterGroups'];
	}*/


	/**
	 * @return array
	 */
	private function createFilterGroupTag($groupId, $groupCode = null)
	{
		$return = [];
		if ($groupId !== null) {
			$return[] = 'filterGroup/' . $groupId;
		}
		if ($groupCode !== null) {
			$return[] = 'filterGroupCode/' . $groupCode;
		}
		return $return;
	}


	/**
	 * @param int        $groupId
	 *
	 * @return array
	 */
	private function createFilterItemsTag($groupId)
	{
		return ['filterItems/' . $groupId];
	}


	/**
	 * @return void
	 */
	/*public function invalidateFilterGroups()
	{
		$conditions = [Cache::TAGS => $this->createFilterGroupsTag()];

		$this->cache->clean($conditions);
	}*/


	/**
	 * @return void
	 */
	public function invalidateFilterGroup($groupId)
	{
		$conditions = [Cache::TAGS => $this->createFilterGroupTag($groupId)];

		$this->cache->clean($conditions);
	}


	/**
	 * @param int|null  $groupId
	 *
	 * @return void
	 */
	public function invalidateFilterItems($groupId)
	{
		$conditions = [Cache::TAGS => $this->createFilterItemsTag($groupId)];

		$this->cache->clean($conditions);
	}

}
