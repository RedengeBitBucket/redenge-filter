<?php

namespace Redenge\Filter\AdminModule\Presenters;

use Nette\Application\UI\Form;
use Nette\Database\Context;
use Redenge\DB;
use Redenge\Engine\Presenters\BasePresenter;
use Redenge\Environment\Environment;
use Redenge\Filter\AdminModule\Components\IFilterEditControl;
use Redenge\Filter\Storage\FilterStorage;
use Ublaboo\DataGrid\DataGrid;


/**
 * Description of FilterPresenter
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class FilterPresenter extends BasePresenter
{

	/**
	 * @var DB @inject
	 */
	public $db;

	/**
	 * @var Context
	 */
	protected $database;

	/**
	 * @var \Redenge\Filter\Storage\FilterStorage
	 */
	private $filterStorage;

	/**
	 * @var IFilterEditControl
	 */
	protected $editFormFactory;


	public function __construct (Context $database, IFilterEditControl $editFormFactory, FilterStorage $filterStorage)
	{
		$this->database = $database;
		$this->editFormFactory = $editFormFactory;
		$this->filterStorage = $filterStorage;
	}


	/**
	 * Temporary fix.
	 */
	public function startup()
	{
		require_once(ENGINE_PATH . '/component/admin/interface.php');
		$admin = new \admin($this->db);
		if (!$admin->user_interface->user->login())
			$this->presenter->redirectUrl('/admin');

		parent::startup();
	}


	public function createComponentFilterGroupGrid()
	{
		$grid = new DataGrid;
		$grid->setDataSource($this->getGroupData());
		$grid->addColumnText('code', 'Kód');
		$grid->addColumnText('name', 'Název');
		$grid->addColumnText('sort_order', 'Řazení');

		$grid->addAction('edit', '', 'Filter:edit', ['id' => $grid->getPrimaryKey()])
			->setIcon('pencil')
			->setTitle('editovat')
			->setClass('btn btn-xs btn-default');

		$grid->addAction('deleteGroup!', '')
			->setIcon('times')
			->setTitle('odstranit')
			->setClass('btn ajax')
			->setConfirm('Opravdu chcete odstranit položku ,,%s"?', 'name');

		$grid->addToolbarButton('Filter:edit', '')->addAttributes([
			//'href' => $this->link('showNewUserForm!')
		])->setClass('btn btn-xs border')
			->setIcon('plus');

		return $grid;
	}


	public function createComponentEditForm()
	{
		$control = $this->editFormFactory->create();

		return $control;
	}


	/**
	 * @return \Nette\Database\ResultSet
	 */
	private function getGroupData()
	{
		return $this->database->table('filter_group')
			->select('filter_group.id, filter_group.code, filter_group.sort_order, :filter_group_attribute.name')
			->where(':filter_group_attribute.language_id', 1);
	}


	public function handleDeleteGroup($id)
	{
		$this->filterStorage->invalidateFilterGroup($this->id);
		$this->filterStorage->invalidateFilterItems($this->id);

		$this->database->table('filter_group')->where('id = ?', $id)->delete();
		$this['filterGroupGrid']->reload();
	}


	public function actionDefault()
	{
		
	}


	public function actionEdit()
	{
		
	}


	public function renderDefault()
	{
		
	}

}
