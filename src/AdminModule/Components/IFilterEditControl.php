<?php

namespace Redenge\Filter\AdminModule\Components;


/**
 * Description of IFilterEditControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface IFilterEditControl
{

	/**
	 * @return FilterEditControl
	 */
	function create();

}
