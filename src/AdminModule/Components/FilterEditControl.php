<?php

namespace Redenge\Filter\AdminModule\Components;

use Nette\Application\UI\Form;
use Nette\Database\Context;
use Redenge\Engine\Components\BaseControl;
use Redenge\Filter\Storage\FilterStorage;


/**
 * Description of FilterEditControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class FilterEditControl extends BaseControl
{

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var Context
	 */
	protected $database;

	/**
	 * @var \Redenge\Filter\Storage\FilterStorage
	 */
	private $filterStorage;

	private $languages;


	public function __construct(Context $database, FilterStorage $filterStorage)
	{
		$this->database = $database;
		$this->filterStorage = $filterStorage;
		$this->languages = $this->getLanguages();
		parent::__construct();
	}


	public function attached($presenter)
	{
		parent::attached($presenter);
		$this->id = $presenter->getParameter('id');
	}


	public function createComponentForm()
	{
		$form = new Form;
		$form->setAction($this->presenter->link('this', ['id' => $this->id]));

		$groupAttribute = $form->addContainer('filter_group_attribute');
		foreach ($this->languages as $languageId => $language) {
			$container = $groupAttribute->addContainer($languageId);
			$container->addText('name')
				->setAttribute('class', 'form-control')
				->setRequired();
		}

		$group = $form->addContainer('filter_group');
		$group->addText('code', 'Kód')
			->setAttribute('class', 'form-control');
		$group->addText('sort_order', 'Pořadí')
			->setAttribute('class', 'form-control')
			->setValue(0);

		$containterButtons = $form->addContainer('buttons');
		$containterButtons->addSubmit('save', 'Uložit')
			->setAttribute('class', 'btn btn-success mr-2');
		$containterButtons->addButton('back', 'Zpět')
				->setAttribute('class', 'btn btn-primary')
				->setAttribute('onclick', sprintf('window.location.href="%s"', $this->presenter->link('Filter:default')));

		$form->onSuccess[] = [$this, 'onSuccessForm'];

		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = null;
		$renderer->wrappers['label']['container'] = 'div class="col-sm-2"';
		$renderer->wrappers['pair']['container'] = 'div class="form-group row align-items-center"';
		$renderer->wrappers['control']['container'] = 'div class="col-sm-10"';

		if ($this->id) {
			$form->setDefaults($this->getDefaults());
		}

		return $form;
	}


	public function onSuccessForm(Form $form)
	{
		if ($this->id === null) {
			$row = $this->database->table('filter_group')->insert([
				'code' => $form->values->filter_group->code,
				'sort_order' => $form->values->filter_group->sort_order,
				'expand' => 1
			]);
			$this->id = $row->id;
		} else {
			$this->database->table('filter_group')->where('id', $this->id)->update([
				'code' => $form->values->filter_group->code,
				'sort_order' => $form->values->filter_group->sort_order,
				'expand' => 1
			]);
		}
		foreach ($form->values->filter_group_attribute as $languageId => $data) {
			if ($id = $this->database->table('filter_group_attribute')->where('filter_group_id', $this->id)->where('language_id', $languageId)->fetchField('id')) {
				$this->database->table('filter_group_attribute')->where('id', $id)->update($data);
			} else {
				$this->database->table('filter_group_attribute')->insert(array_merge((array) $data, [
					'filter_group_id' => $this->id,
					'language_id' => $languageId,
				]));
			}
		}

		$insertedFilterIds = [];
		foreach ($this->presenter->getHttpRequest()->getPost('filter', []) as $filter) {
			$filterId = isset($filter['filter_id']) ? (int) $filter['filter_id'] : 0;
			$data = [
				'code' => $filter['code'],
				'sort_order' => $filter['sort_order'],
				'filter_group_id' => $this->id
			];
			if ($filterId) {
				$this->database->table('filter')->where('id', $filterId)->update($data);
			} else {
				$row = $this->database->table('filter')->insert($data);
				$filterId = $row->id;
			}
			$insertedFilterIds[] = $filterId;
			foreach ($filter['filter_attribute'] as $languageId => $data) {
				if ($id = $this->database->table('filter_attribute')->where('filter_id', $filterId)->where('language_id', $languageId)->fetchField('id')) {
					$this->database->table('filter_attribute')->where('id', $id)->update($data);
				} else {
					$this->database->table('filter_attribute')->insert(array_merge((array) $data, [
						'filter_id' => $filterId,
						'language_id' => $languageId,
					]));
				}
			}
		}

		$this->database->table('filter')->where('(id NOT ?)', $insertedFilterIds)->delete();

		$this->filterStorage->invalidateFilterGroup($this->id);
		$this->filterStorage->invalidateFilterItems($this->id);

		$this->presenter->redirect('Filter:default');
	}


	/**
	 * @return array
	 */
	private function getLanguages()
	{
		$return = [];

		$languages = $this->database->table('language')->fetchAll();
		foreach ($languages as $language) {
			$return[$language['id']] = $language;
		}

		return $return;
	}


	private function getFilters()
	{
		$return = [];
		foreach ($this->database->table('filter')->select('id, code, sort_order')->where('filter_group_id', $this->id) as $filter) {
			$data = $filter->toArray();
			foreach ($this->database->table('filter_attribute')->select('name, language_id')->where('filter_id', $filter->id) as $attribute) {
				$data['attribute']['name'][$attribute->language_id] = $attribute->name;
			}
			$return[] = $data;
		}

		return $return;
	}


	private function getDefaults()
	{
		if ($this->id === null) {
			return [];
		}

		$return = [];

		$row = $this->database->table('filter_group')->get($this->id);
		if ($row) {
			$return['filter_group']['code'] = $row->code;
			$return['filter_group']['sort_order'] = $row->sort_order;
		}
		$rows = $this->database->table('filter_group_attribute')->where('filter_group_id', $this->id);
		foreach ($rows as $row) {
			$return['filter_group_attribute'][$row->language_id]['name'] = $row->name;
		}

		return $return;
	}


	public function render()
	{
		$this->template->languages = $this->languages;
		$this->template->filters = $this->getFilters();

		parent::render();
	}

}
