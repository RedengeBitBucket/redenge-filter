<?php

namespace Redenge\Filter\AdminModule\DI;

use Nette\Application\IPresenterFactory;
use Nette\DI\CompilerExtension;


class FilterExtension extends CompilerExtension
{

	public function beforeCompile()
	{
		$builder = $this->getContainerBuilder();
		$builder->getDefinition($builder->getByType(IPresenterFactory::class))->addSetup(
			'setMapping',
			[['Filter' => 'Redenge\Filter\AdminModule\Presenters\*Presenter']]
		);

		$this->compiler->loadDefinitions(
			$builder,
			$this->loadFromFile(__DIR__ . '/config.neon')['services']
		);
	}

}
