<?php

namespace Redenge\Filter\FrontModule\DI;

use Nette\DI\CompilerExtension;


/**
 * Description of FilterExtension
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class FilterExtension extends CompilerExtension
{

	/**
	 * {@inheritdoc}
	 */
	public function loadConfiguration()
	{
		parent::loadConfiguration();

		$builder = $this->getContainerBuilder();
		$this->compiler->parseServices($builder, $this->loadFromFile(__DIR__ . '/../config/services.neon'), $this->name);
	}

}
