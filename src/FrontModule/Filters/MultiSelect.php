<?php

namespace Redenge\Filter\FrontModule\Filters;

use Nette\Utils\Strings;
use Redenge\Filter\FrontModule\Entity\Filter;


/**
 * Description of MultiSelect
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class MultiSelect
{

	private $groupId;

	private $groupCode;

	private $groupName;

	/**
	 * @var Filter[]
	 */
	private $items;


	public function __construct($groupId, $groupCode, $groupName)
	{
		$this->groupId = $groupId;
		$this->groupCode = $groupCode;
		$this->groupName = $groupName;
	}


	public function getGroupId()
	{
		return $this->groupId;
	}


	public function getItems()
	{
		return $this->items;
	}


	public function getItemsId()
	{
		$return = [];

		foreach ($this->items as $item) {
			$return[] = $item->getId();
		}

		return $return;
	}


	public function addItem(Filter $filter)
	{
		$this->items[] = $filter;
	}


	/**
	 * @param int $filterId
	 * @return bool
	 */
	public function issetFilter($filterId)
	{
		foreach ($this->items as $item) {
			if ($filterId === $item->getId()) {
				return true;
			}
		}

		return false;
	}


	public function getQuery($filterId = null)
	{
		$return = [];
		foreach ($this->items as $item) {
			if ($filterId === null) {
				$return[] = Strings::webalize($this->groupName) . '[]=' . Strings::webalize($item->getName());
			} else {
				if ($filterId !== $item->getId()) {
					continue;
				}
				return Strings::webalize($this->groupName) . '[]=' . Strings::webalize($item->getName());
			}
		}

		return implode('&', $return);
	}
	
}
