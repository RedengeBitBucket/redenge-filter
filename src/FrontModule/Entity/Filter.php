<?php

namespace Redenge\Filter\FrontModule\Entity;

use Redenge\Engine\Entity\Entity;


/**
 * Description of Filter
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class Filter extends Entity
{

	/**
	 * @var int
	 */
	private $filterGroupId;

	/**
	 * @var code
	 */
	private $code;

	/**
	 * @var int
	 */
	private $sortOrder;

	/**
	 * @var string
	 */
	private $name;


	public function getFilterGroupId()
	{
		return $this->filterGroupId;
	}


	public function getCode()
	{
		return $this->code;
	}


	public function getSortOrder()
	{
		return $this->sortOrder;
	}


	public function getName()
	{
		return $this->name;
	}


	public function setFilterGroupId($filterGroupId)
	{
		$this->filterGroupId = $filterGroupId;
		return $this;
	}


	public function setCode($code)
	{
		$this->code = $code;
		return $this;
	}


	public function setSortOrder($sortOrder)
	{
		$this->sortOrder = $sortOrder;
		return $this;
	}


	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

}
