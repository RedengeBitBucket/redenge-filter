![Redenge logo](http://redenge.cz/wp-content/uploads/LOGO-REDENGE-internet-solutions.png)
# Redenge Filter
Modul pro použití filtrů

## Požadavky
- verze PHP 5.6 a vyšší
- balíček nette/nette ~2.4

## Instalace
Nejlepší možnost, jak nainstalovat balíček je přes composer  [Composer](http://getcomposer.org/):
1) Do svého composer.json přidejte odkaz na privátní repozitář satis
```sh
"repositories": [
        {
            "type": "composer",
            "url": "https://satis.redenge.biz"
        }
]
```
2) Stáhněte balíček:
```sh
$ composer require redenge/filter
```

3) Do config.neon přidat extension:
```twig
extensions:
	- Redenge\Filter\AdminModule\DI\FilterExtension
```
